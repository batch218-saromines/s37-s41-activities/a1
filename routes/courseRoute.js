const express = require("express");
const courseRouter = express.Router();

const courseController = require('../controllers/courseController.js');

const auth = require('../auth.js');



//START PART 3 
// LIMIT TO AUTHENTICATED ADMIN
router.post("/", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.createCourse(newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

// Create route for de-activation of a course limited to admin user

courseRouter.post('/create', courseController.addCourse)

courseRouter.get('/all', courseController.getAllCourses)

courseRouter.get('/active', courseController.getActiveCourses)

courseRouter.get('/:id', courseController.getCourseById)

courseRouter.patch('/:id/update', auth.verify, courseController.updateCourse);



//START PART 4
router.patch("/:courseId", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

router.patch("/:courseId/update", auth.verify, (req, res) => {
	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.updateCourse(req.params.courseId, newData).then(resultFromController => {
		res.send(resultFromController)
	})
})

module.exports = courseRouter;

// dependencies
const express = require("express");

const router = express.Router();
const User = require("../Models/user.js");
const userController = require("../Controllers/userController.js");

router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
})

router.post("/details", (req, res) => {
	userController.userDetails(req.body).then(result => res.send(result));
})




// S41 ACTIVITY - PART 5
router.post("/enroll", auth.verify, (req, res) =>{

	const newData = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	userController.enroll(req.body, newData).then(
		resultFromController => {
			res.send(resultFromController)
		})
})

router.post("/checkEmail", (req,res) =>{
	userController.checkEmailExist(req.body).then(resultFromController => res.send(resultFromController))
});

router.post("/register",(req,res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController))
})

router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController))
})


module.exports = router;
